<?php

use App\Delivery;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Enum\DeliveryServiceType;

class DeliverySeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $deliveries = self::getAll();

        $now = Carbon::now();
        foreach ($deliveries as &$delivery) {
            $delivery['date'] = $now;
        }

        Delivery::insert($deliveries);
    }

    protected static function getAll()
    {
        return [
            [
                'from' => 'Kazan',
                'to' => 'Moscow',
                'name' => 'Test Name',
                'phone' => '(642) 555-0134',
                'service' => DeliveryServiceType::GET_CACHE,
            ],
            [
                'from' => 'Kazan',
                'to' => 'Paris',
                'name' => 'Your Name',
                'phone' => '(642) 111-0134',
                'service' => DeliveryServiceType::GET_CACHE,
            ]
        ];
    }
}
