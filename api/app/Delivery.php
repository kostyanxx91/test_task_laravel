<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = [
        'from', 'to', 'date',
        'name', 'phone', 'service'
    ];
}