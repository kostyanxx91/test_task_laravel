<?php

namespace App\Enum;

trait EnumTrait
{
    /**
     * Get all existing enum values.
     *
     * @return array|null
     */
    public static function values()
    {
        return array_values(self::extract());
    }

    /**
     * Extract constants to associative array.
     *
     * @return array
     */
    private static function &extract()
    {
        static $list = null;
        if (!$list) {
            try {
                $reflection = new \ReflectionClass(__CLASS__);
            } catch (\ReflectionException $e) {
                //
            }
            $list = $reflection->getConstants();
        }

        return $list;
    }


}