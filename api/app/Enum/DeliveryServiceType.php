<?php

namespace App\Enum;

class DeliveryServiceType
{
    use EnumTrait;

    const GET_CACHE = 1;
    const FREE_DELIVERY = 2;
    const PAY_CARD = 3;
}