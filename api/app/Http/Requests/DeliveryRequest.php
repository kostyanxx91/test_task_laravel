<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enum\DeliveryServiceType;

class DeliveryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required',
            'to' => 'required',
            'name' => 'required',
            'phone' => ['required', 'regex:/^\(\d{3}\)\s\d{3}-\d{4}$/'],
            'service' => [
                'required',
                Rule::in(DeliveryServiceType::values())
            ],
            'date' => 'required|date_format:"Y-m-d"'
        ];
    }
}