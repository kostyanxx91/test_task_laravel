<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DeliveryRequest;
use App\Delivery;

class DeliveryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('per_page', 100);
        $deliveries = Delivery::query();

        return response()->json(
            $deliveries->paginate($perPage)
        );
    }

    /**
     * @param DeliveryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(DeliveryRequest $request)
    {
        $delivery = new Delivery();
        $delivery->fill($request->all());
        $delivery->saveOrFail();

        return response()->json($delivery);
    }

    /**
     * @param $id
     * @param DeliveryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update($id, DeliveryRequest $request)
    {
        $delivery = Delivery::query()
            ->where('id', $id)
            ->firstOrFail();

        $delivery->fill($request->all());
        $delivery->saveOrFail();

        return response()->json($delivery);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delivery = Delivery::query()
            ->where('id', $id)
            ->firstOrFail();

        $delivery->delete();

        return response()->json($delivery);
    }
}
