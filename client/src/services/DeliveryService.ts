import BaseService from './BaseService'
import { Delivery } from '../models/Delivery';
import {toJS} from "mobx";

class DeliveryService extends BaseService {

    async fetchAll() {
        try {
            const response = await this.httpRequest('deliveries', 'GET', {});
            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async updateDelivery(id: number, data: any) {
        try {
            const response = await this.httpRequest(`deliveries/${id}`, 'PATCH', data);
            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async createDevivery(data: any) {
        try {
            const response = await this.httpRequest('deliveries', 'POST', data);
            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async deleteDevivery(id: number) {
        try {
            const response = await this.httpRequest(`deliveries/${id}`, 'DELETE', {});
            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async getAddress(value: string) {
        try {
            const response = await fetch(`https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': "Token 972f7a40e88470013cd7554f68a328625a0fc5ab",
                },
                body: JSON.stringify({
                    "query": value, "count": 5
                }),
            });

            return await response.json();
        } catch (error) {
            throw error
        }
    }
}

export const deliveryService = new DeliveryService();