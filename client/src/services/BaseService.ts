export default class BaseService {

    httpRequest(url: string, method: string, params: any) {
        return fetch(`http://localhost:8000/api/${url}`, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
            },
            body: method!=="GET" ? JSON.stringify(params) : null,
        });
    }
}


