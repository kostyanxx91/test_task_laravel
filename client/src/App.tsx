import React from 'react';
import { DeliveryComponent } from "./components/DeliveryComponent";

const App: React.FC = () => {
  return (
    <div>
        <DeliveryComponent />
    </div>
  );
}

export default App;
