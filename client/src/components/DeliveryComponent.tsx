import {observer} from "mobx-react";
import React, {useEffect} from "react";
import {AppContext} from "../stores/Stores";
import Add from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Delete from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import CSS from 'csstype';
import 'date-fns';
import {Delivery} from '../models/Delivery'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers';
import MaskedInput from 'react-text-mask';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

interface Props {}

const getServiceName = (service: number) => {
    switch (service) {
        case 1:
            return 'Получить наличными';
        case 2:
            return 'Бесплатная доставка';
        case 3:
            return 'Оплата картой';
        default:
            return 'Нет значения';
    }
}

const Loader = () => (
    <div>
        <div style={loaderStyle}>
            <div style={loaderBoxStyle}>
                <CircularProgress size={70}/>
            </div>
        </div>
    </div>
);

export const DeliveryComponent = observer(function (props: Props) {

    const {deliveryStore, pageStore} = React.useContext(AppContext);
    if (pageStore.isLoading) return <Loader/>;

    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col p-3">
                        <Add style={buttonStyle} onClick={deliveryStore.openModal}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>КУДА</th>
                                <th>ОТКУДА</th>
                                <th>ДАТА</th>
                                <th>ИМЯ</th>
                                <th>ТЕЛЕФОН</th>
                                <th>ДОП УСЛУГА</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {deliveryStore.deliveryList.map((row: Delivery) => (
                                <tr key={row.id}>
                                    <td>{row.id}</td>
                                    <td>{row.to}</td>
                                    <td>{row.from}</td>
                                    <td>{row.date}</td>
                                    <td>{row.name}</td>
                                    <td>{row.phone}</td>
                                    <td>{getServiceName(row.service)}</td>
                                    <td>
                                        <Edit style={buttonStyle} onClick={() => {deliveryStore.changeDelivery(row.id)}}/>
                                        <Delete style={buttonStyle} onClick={() => {deliveryStore.deleteDelivery(row.id)}}/>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {deliveryStore.isOpenModal && (
            <Dialog
                open={true}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div style={{width: 450}}>
                    <DialogContent>
                        <div>
                            <label className="modal-header">
                                ДОБАВИТЬ
                            </label>

                            <div>
                                <TextField
                                    className={`w-100 mt2 ${deliveryStore.fieldErrors.from ? 'input-error' : null}`}
                                    value={deliveryStore.currentDelivery.from || ''}
                                    onChange={
                                        (e) => {
                                            deliveryStore.getAutocompleteAddressFrom(e.target.value);
                                            deliveryStore.changeCurrentDelivery("from", e.target.value)
                                        }
                                    }
                                    label="Куда"/>

                                {(deliveryStore.autocompleteAddressesFrom.length>0) && (
                                    <div style={autocompleteStyle}>
                                        <List component="nav" className="w-100">
                                            {deliveryStore.autocompleteAddressesFrom.map((row: any, i: number) => (
                                                <ListItem button key={i} onClick={() => {deliveryStore.setAddressFrom(row.value, "from")}}>
                                                    {row.value}
                                                </ListItem>
                                            ))}
                                        </List>
                                    </div>
                                )}
                            </div>

                            <div>
                                <TextField
                                    className={`w-100 mt2 ${deliveryStore.fieldErrors.to ? 'input-error' : null}`}
                                    value={deliveryStore.currentDelivery.to || ''}
                                    onChange={
                                        (e) => {
                                            deliveryStore.getAutocompleteAddressTo(e.target.value);
                                            deliveryStore.changeCurrentDelivery("to", e.target.value)
                                        }
                                    }
                                    label="Откуда"/>

                                {(deliveryStore.autocompleteAddressesTo.length>0) && (
                                    <div style={autocompleteStyle}>
                                        <List component="nav" className="w-100">
                                            {deliveryStore.autocompleteAddressesTo.map((row: any, i: number) => (
                                                <ListItem button key={i} onClick={() => {deliveryStore.setAddressTo(row.value, "to")}}>
                                                    {row.value}
                                                </ListItem>
                                            ))}
                                        </List>
                                    </div>
                                )}
                            </div>
                            <div className="mt-2">
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        className="w-100"
                                        variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        label="Дата доставки"
                                        value={deliveryStore.currentDelivery.date ? new Date(deliveryStore.currentDelivery.date) : new Date()}
                                        onChange={
                                            (date) => {
                                                deliveryStore.changeCurrentDelivery("date", date)
                                            }
                                        }
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </div>
                            <TextField
                                className={`w-100 ${deliveryStore.fieldErrors.name ? 'input-error' : null}`}
                                value={deliveryStore.currentDelivery.name || ''}
                                onChange={(e) => {deliveryStore.changeCurrentDelivery("name", e.target.value)}}
                                label="Имя"/>
                            <label
                                className="w-100 mt-3" style={{color: '#808080'}}>
                                Телефон
                            </label>
                            <MaskedInput
                                value={deliveryStore.currentDelivery.phone}
                                onChange={(e) => {deliveryStore.changeCurrentDelivery("phone", e.target.value)}}
                                className={`w-100 pt-2 pb-2 ${deliveryStore.fieldErrors.phone ? 'input-error' : null}`}
                                style={{borderWidth: 0}}
                                mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                                showMask
                            />
                            <Select
                                className={`w-100 mt-2 ${deliveryStore.fieldErrors.service ? 'input-error' : null}`}
                                onChange={(e) => {deliveryStore.changeCurrentDelivery("service", e.target.value)}}
                                value={deliveryStore.currentDelivery.service}>
                                <MenuItem value={1}>Получить наличными</MenuItem>
                                <MenuItem value={2}>Бесплатная доставка</MenuItem>
                                <MenuItem value={3}>Оплаттить картой</MenuItem>
                            </Select>
                        </div>
                    </DialogContent>
                    <DialogActions className="p-3 mt-2">
                        <Button
                            color="primary"
                            onClick={deliveryStore.closeModal}
                            className="modal-button-cancel">
                            ОТМЕНА
                        </Button>
                        <Button
                            color="primary"
                            onClick={deliveryStore.saveDelivery}
                            className="modal-button-cancel">
                            СОХРАНИТЬ
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>)}
        </div>
    );
});

const loaderStyle: CSS.Properties = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    background: 'white',
    zIndex: 2
};

const loaderBoxStyle: CSS.Properties = {
    width: '70px',
    margin: '0 auto',
    marginTop: '20%'
};

const buttonStyle: CSS.Properties = {
    cursor: 'pointer',
};

const autocompleteStyle: CSS.Properties = {
    position: 'absolute',
    background: '#efefef',
    width: '89%',
    zIndex: 200
};
