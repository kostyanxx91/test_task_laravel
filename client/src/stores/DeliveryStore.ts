import { observable, action, toJS } from 'mobx';
import { deliveryService } from '../services/DeliveryService'
import {
    Deliveries,
    Delivery,
    DeliveryValidateFields,
    DeliveryRequireFields
} from '../models/Delivery'

export class DeliveryStore {

    @observable deliveryList: Deliveries = [];
    @observable autocompleteAddressesTo: string[] = [];
    @observable autocompleteAddressesFrom: string[] = [];
    @observable currentDelivery: Delivery = <Delivery>{};
    @observable fieldErrors: DeliveryValidateFields = <DeliveryValidateFields>{};
    @observable isOpenModal = false;

    pageStore: any;

    constructor(pageStore: any) {
        this.pageStore = pageStore;
        this.currentDelivery.service = 1;
        this.currentDelivery.date = new Date();
        this.fetchAll();
    }

    async fetchAll() {
        this.pageStore.startLoading();
        const deliveries = await deliveryService.fetchAll();

        if (deliveries['data']) {
            this.deliveryList = deliveries['data'];
            this.pageStore.endLoading();
        }
    }

    async fetchAutocompleteAddressTo(value: string) {
        const addresses = await deliveryService.getAddress(value);

        if (addresses['suggestions']) {
            this.autocompleteAddressesTo = addresses['suggestions'];
        } else {
            this.autocompleteAddressesTo = [];
        }
    }

    async fetchAutocompleteAddressFrom(value: string) {
        const addresses = await deliveryService.getAddress(value);

        if (addresses['suggestions']) {
            this.autocompleteAddressesFrom = addresses['suggestions'];
        } else {
            this.autocompleteAddressesFrom = [];
        }
    }

    getZeroDate(value: number) {
        if (value <= 9) {
            return 0;
        }

        return '';
    }

    @action
    changeCurrentDelivery = (deliveryField: string, value: any) => {
        this.currentDelivery[deliveryField] = value;
    };

    @action
    saveDelivery = async () => {
        this.pageStore.startLoading();

        const date = this.currentDelivery.date;
        const modifyDate = `${date.getFullYear()}-${this.getZeroDate(
                date.getMonth()+1
            )}${date.getMonth()+1}-${this.getZeroDate(
                date.getDate()
            )}${date.getDate()}`;

        if (this.currentDelivery['id']) {
            await deliveryService.updateDelivery(
                    this.currentDelivery['id'],
                    {...this.currentDelivery, date : modifyDate}
                );
        } else {
            await deliveryService.createDevivery({...this.currentDelivery, date : modifyDate});
        }
        this.pageStore.endLoading();
        this.closeModal();
        this.fetchAll();
    };

    @action
    changeDelivery = (id: number):void => {
        const delivery: Delivery = <Delivery>this.deliveryList.find((row) => row.id === id);
        this.currentDelivery = <Delivery>{...delivery};
        this.isOpenModal = true;
    };

    @action
    deleteDelivery = async (id: number) => {
        this.pageStore.startLoading();
        await deliveryService.deleteDevivery(id);
        this.pageStore.endLoading();
        this.fetchAll();
    };

    @action
    openModal = () => {
        this.isOpenModal = true;
    };

    @action
    closeModal = () => {
        this.isOpenModal = false;
        this.currentDelivery = <Delivery>{};
        this.currentDelivery.service = 1;
        this.currentDelivery.date = new Date();
    };

    @action
    getAutocompleteAddressTo = (value: string) => {
        this.fetchAutocompleteAddressTo(value);
    };

    @action
    setAddressTo = (value: string, field: string) => {
        this.currentDelivery[field] = value;
        this.autocompleteAddressesTo = [];
    };

    @action
    getAutocompleteAddressFrom = (value: string) => {
        this.fetchAutocompleteAddressFrom(value);
    };

    @action
    setAddressFrom = (value: string, field: string) => {
        this.currentDelivery[field] = value;
        this.autocompleteAddressesFrom = [];
    };
}