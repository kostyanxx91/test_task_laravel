import * as React from "react";
import { DeliveryStore } from './DeliveryStore';
import { PageStore } from './PageStore';

export function createStores() {

    const pageStore = new PageStore();

    return {
        pageStore,
        deliveryStore: new DeliveryStore(pageStore)
    };
}

export const stores = createStores();

export const AppContext = React.createContext(stores);