import { observable, action } from 'mobx';

export class PageStore {

    @observable isLoading = false;

    @action
    startLoading = () => {
        this.isLoading = true;
    };

    @action
    endLoading = () => {
        this.isLoading = false;
    }
}