export declare type Deliveries = Delivery[]

export interface Delivery {
    id: number
    to: string
    from: string
    date: Date
    name: string
    phone: string
    service: number
    created_at: string
    updated_at: string
    [index: string]: any
}

export interface DeliveryValidateFields {
    to: boolean
    from: boolean
    date: boolean
    name: boolean
    phone: boolean
    service: boolean
}

export const DeliveryRequireFields: DeliveryValidateFields = {
    to: true,
    from: true,
    date: true,
    name: true,
    phone: true,
    service: true,
};