- BACK

Для запуска необходимо (php, composer, mysql)

- настроить env конфиг
- выполнить php artisan migrate:fresh --seed
- запустить тестовый апи сервер php artisan serve

- FRONT

Для запуска необходимо npm последней версии

- перейти в client
- выполнить npm install
- выполнить npm start

По адресу localhost:3000 будет доступен тестовый сервер